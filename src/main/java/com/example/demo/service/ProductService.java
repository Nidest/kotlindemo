package com.example.demo.service;

import com.example.demo.model.internal.Product;
import com.example.demo.model.internal.ProductType;
import com.example.demo.repository.ProductRepository;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ProductService {

  private ProductRepository repository;

  public ProductService(ProductRepository repository) {
    this.repository = repository;
  }

  public Product saveProduct(Product product) {
    return repository.save(product);
  }

  public List<Product> getAllProducts() {
    return repository.findAll();
  }

  public List<Product> getProductsOfTypes(List<ProductType> productTypes) {
    return repository.findAll()
        .stream()
        .filter(product -> productTypes.contains(product.getProductType()))
        .collect(Collectors.toList());
  }
}
