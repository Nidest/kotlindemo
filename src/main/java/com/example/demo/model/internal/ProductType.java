package com.example.demo.model.internal;

public enum ProductType {
  LAPTOP,
  SMART_PHONE,
  DUMB_PHONE,
  DESKTOP,
  TABLET
}
