package com.example.demo.model.external;

import java.util.Objects;

public class ProductDto {
  private String id;
  private String name, description;
  private double price;
  private ProductTypeDto productType;

  public ProductDto() {
  }

  public ProductDto(String id, String name, String description, double price, ProductTypeDto productType) {
    this.id = id;
    this.name = name;
    this.description = description;
    this.price = price;
    this.productType = productType;
  }

  public String getId() {
    return id;
  }

  public String getName() {
    return name;
  }

  public String getDescription() {
    return description;
  }

  public double getPrice() {
    return price;
  }

  public ProductTypeDto getProductType() {
    return productType;
  }

  public void setId(String id) {
    this.id = id;
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public void setPrice(double price) {
    this.price = price;
  }

  public void setProductType(ProductTypeDto productType) {
    this.productType = productType;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    ProductDto product = (ProductDto) o;
    return Double.compare(product.price, price) == 0 &&
        Objects.equals(id, product.id) &&
        Objects.equals(name, product.name) &&
        Objects.equals(description, product.description) &&
        productType == product.productType;
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, name, description, price, productType);
  }

  @Override
  public String toString() {
    return "Product{" +
        "id=" + id +
        ", name='" + name + '\'' +
        ", description='" + description + '\'' +
        ", price=" + price +
        ", productType=" + productType +
        '}';
  }
}
