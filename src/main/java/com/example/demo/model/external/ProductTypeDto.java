package com.example.demo.model.external;

public enum ProductTypeDto {
  LAPTOP,
  SMART_PHONE,
  DUMB_PHONE,
  DESKTOP,
  TABLET
}
