package com.example.demo.controller;


import com.example.demo.mapper.ProductDtoMapper;
import com.example.demo.mapper.ProductMapper;
import com.example.demo.model.external.ProductDto;
import com.example.demo.model.external.ProductTypeDto;
import com.example.demo.model.internal.Product;
import com.example.demo.model.internal.ProductType;
import com.example.demo.service.ProductService;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
public class ProductController {

  private ProductService productService;

  public ProductController(ProductService productService) {
    this.productService = productService;
  }

  @PostMapping(consumes = {"application/json"}, produces = {"application/json"})
  ProductDto saveProduct(@RequestBody ProductDto productDto) {
    Product product = ProductDtoMapper.map(productDto);
    return ProductMapper.map(productService.saveProduct(product));
  }

  @GetMapping(produces = {"application/json"})
  List<ProductDto> getAllProducts() {
    return productService.getAllProducts()
        .stream()
        .map(ProductMapper::map)
        .collect(Collectors.toList());
  }

  @GetMapping(value = "/types", produces = {"application/json"})
  List<ProductDto> getProductsOfTypes(@RequestParam("names") List<ProductTypeDto> productTypeDtos) {
    List<ProductType> productTypes = productTypeDtos.stream()
        .map(ProductTypeDto::name)
        .map(ProductType::valueOf)
        .collect(Collectors.toList());

    return productService.getProductsOfTypes(productTypes)
        .stream()
        .map(ProductMapper::map)
        .collect(Collectors.toList());
  }
}
