package com.example.demo.mapper;

import com.example.demo.model.external.ProductDto;
import com.example.demo.model.external.ProductTypeDto;
import com.example.demo.model.internal.Product;
import com.example.demo.model.internal.ProductType;
import org.bson.types.ObjectId;

public class ProductDtoMapper {

    public static Product map(ProductDto productDto) {
        return new Product(
                objectId(productDto.getId()),
                productDto.getName(),
                productDto.getDescription(),
                productDto.getPrice(),
                productType(productDto.getProductType())
        );
    }

    private static ProductType productType(ProductTypeDto productTypeDto) {
        if (productTypeDto == null) {
            return null;
        }

        return ProductType.valueOf(productTypeDto.name());
    }

    private static ObjectId objectId(String id) {
        if (id == null) {
            return new ObjectId();
        } else if (id.isEmpty()) {
            return new ObjectId();
        } else {
            return new ObjectId(id);
        }
    }
}
