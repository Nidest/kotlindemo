package com.example.demo.mapper;

import com.example.demo.model.external.ProductDto;
import com.example.demo.model.external.ProductTypeDto;
import com.example.demo.model.internal.Product;
import com.example.demo.model.internal.ProductType;
import org.bson.types.ObjectId;

public class ProductMapper {
  public static ProductDto map(Product product) {
    return new ProductDto(
            productId(product.getId()),
            product.getName(),
            product.getDescription(),
            product.getPrice(),
            productTypeDto(product.getProductType())
    );
  }

  private static String productId(ObjectId id) {
    if (id == null) {
      return null;
    }

    return id.toHexString();
  }

  private static ProductTypeDto productTypeDto(ProductType productType) {
    if (productType == null) {
      return null;
    }

    return ProductTypeDto.valueOf(productType.name());
  }
}
