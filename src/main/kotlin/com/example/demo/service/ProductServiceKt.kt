package com.example.demo.service;

import com.example.demo.model.internal.ProductKt
import com.example.demo.model.internal.ProductTypeKt
import com.example.demo.repository.ProductRepositoryKt
import org.springframework.stereotype.Service

@Service
internal class ProductServiceKt(private val repository: ProductRepositoryKt) {

    fun saveProduct(product: ProductKt) = repository.save(product)

    fun getAllProducts() = repository.findAll()

    fun getProductsOfTypes(productTypes: List<ProductTypeKt>) =
            repository.findAll().filter { type -> productTypes.contains(type.productType) }
}
