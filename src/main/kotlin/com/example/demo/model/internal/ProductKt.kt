package com.example.demo.model.internal

import org.bson.types.ObjectId
import org.springframework.data.mongodb.core.mapping.Document

@Document("products")
internal class ProductKt(
  val id: ObjectId?,
  val name: String,
  val description: String?,
  val price: Double = 0.0,
  val productType: ProductTypeKt?
)
