package com.example.demo.model.internal;

internal enum class ProductTypeKt {
    LAPTOP,
    SMART_PHONE,
    DUMB_PHONE,
    DESKTOP,
    TABLET;
}
