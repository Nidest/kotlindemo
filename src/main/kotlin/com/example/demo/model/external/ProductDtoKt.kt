package com.example.demo.model.external

internal data class ProductDtoKt(
  val id: String?,
  val name: String,
  val description: String?,
  val price: Double = 0.0,
  val productType: ProductTypeDtoKt?
)
