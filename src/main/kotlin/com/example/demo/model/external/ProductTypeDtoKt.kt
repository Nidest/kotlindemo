package com.example.demo.model.external;

internal enum class ProductTypeDtoKt {
  LAPTOP,
  SMART_PHONE,
  DUMB_PHONE,
  DESKTOP,
  TABLET;
}
