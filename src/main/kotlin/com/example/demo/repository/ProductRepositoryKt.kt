package com.example.demo.repository

import com.example.demo.model.internal.ProductKt
import org.bson.types.ObjectId
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.stereotype.Repository

@Repository
internal interface ProductRepositoryKt : MongoRepository<ProductKt, ObjectId>