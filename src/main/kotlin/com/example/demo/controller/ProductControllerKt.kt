package com.example.demo.controller;


import com.example.demo.mapper.map
import com.example.demo.model.external.ProductDtoKt
import com.example.demo.model.external.ProductTypeDtoKt
import com.example.demo.model.internal.ProductKt
import com.example.demo.model.internal.ProductTypeKt
import com.example.demo.service.ProductServiceKt
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("kotlin")
internal class ProductControllerKt(val productService: ProductServiceKt) {

  @PostMapping(consumes = ["application/json"], produces = ["application/json"])
  fun saveProductKt(@RequestBody productDto: ProductDtoKt): ProductDtoKt =
    productService.saveProduct(productDto.map()).map()

  @GetMapping(produces = ["application/json"])
  fun getAllProductKts(): List<ProductDtoKt> = productService.getAllProducts().map(ProductKt::map)

  @GetMapping("/types", produces = ["application/json"])
  fun getProductKtsOfTypes(@RequestParam("names") productTypeDtos: List<ProductTypeDtoKt>): List<ProductDtoKt> {
    val types = productTypeDtos
      .map { it.name }
      .map(ProductTypeKt::valueOf)

    return productService.getProductsOfTypes(types).map(ProductKt::map)
  }
}
