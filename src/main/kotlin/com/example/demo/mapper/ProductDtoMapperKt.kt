package com.example.demo.mapper;

import com.example.demo.model.external.ProductDtoKt
import com.example.demo.model.internal.ProductKt
import com.example.demo.model.internal.ProductTypeKt
import org.bson.types.ObjectId

internal fun ProductDtoKt.map() = ProductKt(
        id = if (id == null) null else ObjectId(id),//id?.let(::ObjectId),
        name = name,
        description = description,
        price = price,
        productType = productType?.name?.let { type ->
            ProductTypeKt.valueOf(type)
        }
)
