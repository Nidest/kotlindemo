package com.example.demo.mapper;

import com.example.demo.model.external.ProductDtoKt
import com.example.demo.model.external.ProductTypeDtoKt
import com.example.demo.model.internal.ProductKt

internal fun ProductKt.map() = ProductDtoKt(
  id = id?.toHexString(),
  name = name,
  description = description,
  price = price,
  productType = productType?.name?.let { ProductTypeDtoKt.valueOf(it) }
)
